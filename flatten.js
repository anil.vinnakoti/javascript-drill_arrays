const each = require('./each')

function flatten(items) {
  if(Array.isArray(items)){
    const flattenArray = [];
    each(items, item => {
      if (Array.isArray(item)) {
        flattenArray.push(...flatten(item));
      } else {
        flattenArray.push(item);
      }
    });
    return flattenArray;
  }
  else{
    return 'This method works only for an Array.';
  }

}

module.exports = flatten;