function find(array, callback){
    if(Array.isArray(array)){
      let newArray = [];
      for(let index=0;index<array.length;index++){
        if(callback(array[index], index, array)){
          newArray.push((array[index]));
        };
      };
      return newArray[0];
    }
    else{
      return 'This method works only for an Array.'
    }
  };

  module.exports = find;