function reduce(array, callback) {
    if(Array.isArray(array)){

        let accumulator = 0;
        for(let index=0;index<array.length;index++){
            accumulator = callback(accumulator, array[index]);
        }
        return accumulator;
    }
    else {
        return 'This method work for Array only'
    }

};

module.exports = reduce;