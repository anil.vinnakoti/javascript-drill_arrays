
function map(array, callback) {
    if(Array.isArray(array)){
      const newArray = [];
      for (let index = 0; index < array.length; index++) {
        newArray.push(callback(array[index], index, array));
      };
      return newArray;
    }
    else{
      return 'This method works only for an Array.';
    }
    };
  
  module.exports = map;