const reduce = require('../reduce')

const items = [1, 2, 3, 4, 5, 5];

function reducingAnArray(previousValue, current_value){
    return previousValue + current_value;
}

const sum = reduce(items, reducingAnArray);
console.log(sum);