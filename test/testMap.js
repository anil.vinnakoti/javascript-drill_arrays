const map = require('../map')

const items = [1, 2, 3, 4, 5, 5];

const result = map(items, function (value, index, array) {
  return array[index] * 3;
});
console.log(result);


//to test the recreated function is same as actual method

const actualResult = items.map((element, index, array) => array[index] * 3)
console.log(actualResult);

let matched = true;
for(let index=0; index<actualResult.length; index++){
  if(result[index] !== actualResult[index]){
    matched = false;
    break;
  }
};

matched ? console.log('Both results are same.') : console.log('Results are not same.');