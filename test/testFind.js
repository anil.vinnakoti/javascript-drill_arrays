const find = require('../find')

const items = [1, 2, 3, 4, 5, 5];

function findingAnElement(input, index, array) {
  return array[index] == 3;
};


//to test the recreated function is same as actual method

const result = find(items, findingAnElement);
console.log(result);

const actualResult = items.find((element, index, array) => array[index]== 3)
console.log(actualResult);

let matched = true;
for(let index=0; index<actualResult.length; index++){
  if(result[index] !== actualResult[index]){
    matched = false;
    break;
  }
};

matched ? console.log('Both results are same.') : console.log('Results are not same.');