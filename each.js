function each(array, callback){
    if(Array.isArray(array)){
        for(let index = 0;index<array.length;index++){
            callback(array[index], index)
        };
    }
    else {
        console.log ('This method work for Array only')
    }

};

module.exports = each;